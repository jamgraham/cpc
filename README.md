# Content Playlist Creation

## How to quickly test
This assumes you already have rvm installed (https://rvm.io/) to manage ruby versions. If not install it then install ruby version 2.3.3, `rvm install 2.3.3`

After that you can rake install the gem, enter into the console, require the gem, and start using it.

    $ git clone git@bitbucket.org:jamgraham/cpc.git
    $ cd cpc
    $ gem install bundler
    $ bundle
    $ rake install
    $ ruby run_program.rb

Results:
```
Input: 'MI3', 'CA'
Output: [["V5", "V1"]]
--
Input: 'MI3', 'UK'
Output: [["V6", "V2"], ["V7", "V3"]]
```

## Assumptions
- input cast to strings
- output cast to arrays
- JSON files are cached and loaded once pre filename and should be loaded on server boot once and only once.

## To Do
- [ ] Use standard error codes
- [ ] Discuss converting all response to json
- [ ] Decouple get_valid_prerolls from @videos to improve multi-threading capacity
- [ ] Add tests for each model to include validation errors
- [ ] Add positive and negative tests for each playlist method
- [ ] Add validations for creating models once and preventing duplication

## Assignment

- [x] Design object models for these.

- [x] Define and create models in memory from data defined in a file(in JSON format). Assume large file.

- [x] Write a program that will take as input a content identifier and return a list of valid playlists applicable.

- [x] We want to understand your thought process and decisions, write these as you go.

- [x] Write code as if you are going to offer this as a library to be utilized by an API server, and describe what that means to you.
    - Code used by an API server should be versionable and packaged.
    - Versions:
        - It's important to keep track which package version a production server is using to avoid conflicts with dependencies. Code and libraries change versions quickly and may not be backwards compatible with existing libraries. Versioning is very important.
    - Packaged:
        - Sharing code between servers through a package makes it easy. It takes one import line to share a library and get the libraries functionality.

- [x] Treat it as production code and describe what that means to you.
    - Production code to me means a versionable library or package that can be used by a service.
    - It also means code that has been load tested and meets requirements for the service it's being used in.

- [x] Provide some convenient means to execute the code
    - `run_program.rb` is the easiest way to execute this library
    - Several methods are exposed to the consumer of the library. Mainly the `load` and `get_playlist` methods.

- [x] If you have made assumptions, include those in the comments.

- [x] For purposes of the interview, please utilize concurrency in the solution even though this could be accomplished without.



## Definitions

### Content
This entity represents a piece of content (an asset that has been licensed by Netflix) that is either a movie or an episode of a TV
show. A piece of Content may have 0 or more ordered Pre-Rolls associated with it.

### Pre-Roll
This represents an asset to be played before the Content plays (e.g. a video clip that shows "Netflix Original Presentation"). A
Pre-Roll may be associated with 0 or more pieces of Content. When a Netflix customer chooses to play a piece of Content, the Pre-Rolls
associated with the Content will be played in correct order followed by the Content.

### Video
This is the actual Video file attached to a Content or a Pre-Roll. Each Content or Pre-Roll can have multiple Videos attached to it.
Each attached Video is uniquely tagged with a language, a list of countries and an aspect ratio.

### Playlist
A legal sequence of Pre-Rolls and Content Videos in the correct order as instructions to a player. The included Videos for Content and
all defined Pre-Rolls will all have matching attributes (at least one country and language in common and the same aspect ratio) to form a
legal play sequence as defined when capturing the Content to Pre-Roll relationship.

### Sample Input file
```
{
  "content": [
    {
      "name": "MI3",
      "preroll": [{ "name": "WB1" }],
      "videos": [
        { "name": "V1", "attributes": {"countries": ["US", "CA"], "language":"English", "aspect": "16:9"} },
        { "name": "V2", "attributes": {"countries": ["UK"], "language": "English","aspect": "4:3"} },
        { "name": "V3", "attributes": {"countries": ["UK"], "language": "English","aspect": "16:9"} }
      ]
    }
  ],
  "preroll": [
    {
      "name": "WB1",
      "videos": [
        { "name": "V4", "attributes": {"countries": ["US"], "language": "English","aspect": "4:3"} },
        { "name": "V5", "attributes": {"countries": ["CA"], "language": "English","aspect": "16:9"} },
        { "name": "V6", "attributes": {"countries": ["UK"], "language": "English","aspect": "4:3"} },
        { "name": "V7", "attributes": {"countries": ["UK"], "language": "English","aspect": "16:9"} }
      ]
    }
  ]
}
```

### Sample Runs

Input (MI3, US)
Output
  (No legal playlist possible because the Pre-Roll Video isn't compatible with the aspect ratio of the Content Video for the US)

Input (MI3, CA)
Output
  Playlist1
  {V5, V1}

Input (MI3, UK)
Output
  Playlist1
  {V6, V2}
  Playlist2
  {V7, V3}

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'content_playlist_creation'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install content_playlist_creation

## Usage

- Load input using the Content Playlist Creation `load` method.
- Get a set of valid playlists by passing arguments into the `get_valid_playlists` method

```
@playlist = ContentPlaylistCreation.load("#{File.dirname(__FILE__)}/fixtures/valid_input.json")
result = @playlist.get_valid_playlists('MI3', 'CA')
>> [["V5", "V1"]]
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/content_playlist_creation.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Spec sample output
```
ContentPlaylistCreation
  has a version number
  #load
    load video model with valid json
    load video model with invalid json
  #get_valid_playlists
    valid
      should return valid playlist for 'MI3', 'CA' input
      should return valid playlist for 'MI3', 'UK' input
      should return valid playlist for 'XI9', 'BA' input
    invalid
      should return invalid playlist for 'MI3', 'UK' input
      should return ArgumentError for invalid country for 'MI3', 'XX' input
      should return ArgumentError for invalid playlist for 'MI3', 'XX' input
      should return ArgumentError for nil input

Video
  #validation
    video
      is created with valid json

Finished in 0.00628 seconds (files took 0.18799 seconds to load)
11 examples, 0 failures
```