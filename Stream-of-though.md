Alright let’s get started. I've read the assignment and it looks straight forward for the most part. I'll have to make some assumptions but I can document those. Things looks good.

First I need to select a langue to write the solution in. The requirements define that the solution be run on a server so whatever langue I choose should be able to create packages/libraries/gems etc.

Given that one of the requirements limit the time I can spend on this I'm going to choose the langue i'm quickest in, which is Ruby. It meets the requirements of executing server code in that it can be packaged into a gem and distributed to other servers. The gem could be exposed through an API as well if this functionality ever needed to be exposed via REST.

Now I'll need to design models for the json. I'm just going to take the json keys and translate those into models. For the most part this makes sense. I can see some clear model definitions within in the json so this strategy seems to make sense.

There, I have models for the json: video, preroll, content, attributer, and a playlist to import everything.

It's great to have models for this. In the future I can add additional validation checks and make use of the all the great features I’m provided through ActiveRecord. This should help when additional requirements come in as I’m sure this package is going to get a lot of use. Having models will also let me add specs/tests to each one. That way I can get pretty granular with my testing strategy. Since this library is going to be used across our server stack testing will be very important.

Now it's time to load the file. This poses some weird problems. Since I’m not persisting the models or the json file everything is going to live in memory. No data store at all. That means each server will need to load the json file in memory for itself to use. I'll need to make sure that the json is loaded into memory only once. Maybe we can use redis or another in memory shared db in the future

Hmm, what if the client wants to load multiple json files? Hmm I can easily make a key/value object and use the json path as the key and use the json as the value. That will work for now until additional requirements are provided.

Alright. Now that the json is loaded and I have models populated I’m ready to write the logic to compare the content playlist names requested from the input with my model data.

Yikes! I'm going to have to loop over my object models a lot. That's really going to slow down performance. If I had extra time I would spend it focusing on improving the way I iterate through my model relationships to match videos and prerolls. I'll use the trusty map method for now but I'll make a note to come back to this later. Here's a good place for threading. At lease I could get some quick gains with that. 

Great. So now when I test the application with the inputs provided my application returns the expected output.

I'm going to write a few more positive and negative tests.

Hmm. Something still feels weird. I'm going to add a few more items to the sample input json (which is very small) and see if my tests break. 

Yep they did. Ok - I need to do some more matching with content name and aspect ratio when I check for prerolls.

Ok great. Now all my tests pass.

I wish I had more tests. I always say that but it's always true. 

Ok well for now this is going to work. The specs pass. Input and output match the requirements. I did translate the output into arrays but I noted that as a requirement. I think this output is easier for other apps to interact with so i hope it's ok.

Ok - all done for now. 

