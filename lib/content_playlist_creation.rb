require 'active_model'
require 'json'
require 'logger'
require "content_playlist_creation/version"
require "content_playlist_creation/playlist"
require "content_playlist_creation/models/attributes"
require "content_playlist_creation/models/content"
require "content_playlist_creation/models/preroll"
require "content_playlist_creation/models/video"


module ContentPlaylistCreation

  # Load and return a new playlist
  @logger = Logger.new(STDOUT)
  @logger.level = Logger::WARN

  @playlists = {}

  def self.get_playlist(path_to_json)
    raise StandardError, "playlist file not loaded" if !@playlists[path_to_json]
    @playlists[path_to_json]
  end

  # Cache the playlist in memory
  # and only load it once
  def self.load(path_to_json)
    if !@playlists[path_to_json]
      @logger.debug("ContentPlaylistCreation.load :: Creating new playlist")
      json = JSON.parse(File.read(path_to_json))
      @playlists[path_to_json] = Playlist.new(json)
    else
      @logger.debug("ContentPlaylistCreation.load :: Using cached playlist")
    end
    @playlists[path_to_json]
  end

end
