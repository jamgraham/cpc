# Video Attributes

# { "name": "V1", "attributes": {"countries": ["US", "CA"], "language":"English", "aspect": "16:9"} },

class Attributes
  include ActiveModel::Model

  attr_accessor :countries, :language, :aspect

  validates :countries, presence: true
  validates :language, presence: true
  validates :aspect, presence: true

  def initialize(attributes)
    @countries = attributes['countries']
    @language = attributes['language']
    @aspect = attributes['aspect']
  end

  def countries
    @countries
  end
end
