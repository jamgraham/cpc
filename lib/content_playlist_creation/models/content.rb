### Content
# This entity represents a piece of content (an asset that has been licensed by Netflix) that is either a movie or an episode of a TV
# show. A piece of Content may have 0 or more ordered Pre-Rolls associated with it.

# "content": [
#   {
#     "name": "MI3",
#     "preroll": [{ "name": "WB1" }],
#     "videos": [
#       { "name": "V1", "attributes": {"countries": ["US", "CA"], "language":"English", "aspect": "16:9"} },
#       { "name"


class Content
  include ActiveModel::Model

  attr_accessor :name, :preroll, :videos

  validates :name, presence: true
  validates :attributes, presence: true

  # has array of prerolls and videos

  def initialize(content)
    @videos = []
    @content = []
    @preoll = []

    @name = content[:name]
    @preroll = content[:preroll]
    @videos.push content[:videos].map{|video| Video.new(video)}
    @videos.flatten!.compact!
  end

  def name
    @name
  end

  def content
    @conent
  end

  def videos
    @videos
  end

end
