# Pre-Roll
# This represents an asset to be played before the Content plays (e.g. a video clip that shows "Netflix Original Presentation"). A
# Pre-Roll may be associated with 0 or more pieces of Content. When a Netflix customer chooses to play a piece of Content, the Pre-Rolls
# associated with the Content will be played in correct order followed by the Content.

class Preroll
  include ActiveModel::Model

  attr_accessor :name, :videos

  validates :name, presence: true
  validates :videos, presence: true

  # has array of videos

  def initialize(preroll)
    @videos = []
    @name = preroll[:name]
    @videos.push preroll[:videos].map{|video| Video.new(video)}
    @videos.flatten!.compact!
  end
end
