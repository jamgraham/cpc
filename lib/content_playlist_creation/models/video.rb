# Video
# This is the actual Video file attached to a Content or a Pre-Roll. Each Content or Pre-Roll can have multiple Videos attached to it.
# Each attached Video is uniquely tagged with a language, a list of countries and an aspect ratio.

# "videos": [
# { "name": "V1", "attributes": {"countries": ["US", "CA"], "language":"English", "aspect": "16:9"} },

class Video
  include ActiveModel::Model

  attr_accessor :name, :attributes

  validates :name, presence: true
  validates :attributes, presence: true

  def initialize(video)
    @name = video['name']
    @attributes = Attributes.new(video['attributes'])
  end
end
