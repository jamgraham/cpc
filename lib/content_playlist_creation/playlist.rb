# Playlist
# A legal sequence of Pre-Rolls and Content Videos in correct order as instructions to a player. The included Videos for Content and
# all defined Pre-Rolls will all have matching attributes (at least one country and language in common and the same aspect ratio) to form a
# legal play sequence as defined when capturing the Content to Pre-Roll relationship.
require "thwait"
class Playlist

  # TODO: Internationalize. Move to yml config and fetch lang needed
  NO_PLAYLIST_ERROR = "(No legal playlist possible because the Pre-Roll Video isn't compatible with the aspect ratio of the Content Video for the US)"

  include ActiveModel::Model

  attr_accessor :name, :attributes

  validates :name, presence: true
  validates :attributes, presence: true

  def get_valid_playlists(content_name, country)
    validate_params(content_name, country)
    threads = []

    # TODO: Refactory @videos dependency out of get_valid_prerolls
    #  Then move get_valid_x into seperate threads
    threads << Thread.new do
      @videos = get_valid_videos(content_name, country)
      @prerolls = get_valid_prerolls(content_name,country)
    end
    threads.map(&:join)

    ThreadsWait.all_waits(threads) do |t|
      # wait for threads to complete before returning result
    end

    return NO_PLAYLIST_ERROR if @prerolls.count == 0
    @prerolls.map(&:name).zip(@videos.map(&:name)).compact
  end

  def get_valid_videos(content_name, country)
    # I would spend more time focusing on efficency here
    @content.select{|c| c.name==content_name }.flat_map{|c| c.videos.to_a.select{|v| v.attributes.countries.include?(country) }}
  end

  def get_valid_prerolls(content_name,country)
    video_aspect_ratio = @videos.map{|v| v.attributes.aspect}
    preroll_name = @content.select{|a| a.name==content_name }.flat_map{|c| c.preroll.map{|p| p['name']}}

    # Same here, not the most efficient but it works. Great for a POC but would need work for production
    @preroll.select{|c| preroll_name.include?(c.name) }.flat_map{|c| c.videos.to_a.select{|v| v.attributes.countries.include?(country) }}.select{|v| video_aspect_ratio.include?(v.attributes.aspect)}
  end

  def content=(content_list)
    @content = []
    # So easy to create new objects of Content
    content_list.each do |content|
      @content.push(
        Content.new(
          name: content['name'],
          preroll: content['preroll'],
          videos: content['videos']
        )
      )
    end
  end

  def preroll=(preroll_list)
    @preroll = []
    # So easy to create new objects of Prerolls
    preroll_list.each do |preroll|
      @preroll.push(
        Preroll.new(
          name: preroll['name'],
          videos: preroll['videos']
        )
      )
    end
  end

  def content
    @content
  end

  def preroll
    @preroll
  end

  private

  def validate_params(content_name, country)
    threads = []
    threads << Thread.new do
      if @content.select{|c| c.name==content_name }.count == 0
        raise ArgumentError.new("content_name is invalid")
      end
    end
    threads << Thread.new do
      if @content.select{|c| c.name==content_name }.flat_map{|c| c.videos.to_a.select{|v| v.attributes.countries.include?(country) }}.count == 0
        raise ArgumentError.new("country is invalid")
      end
    end
    threads.map(&:join)
  end

end
