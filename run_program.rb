require 'content_playlist_creation'
require "content_playlist_creation/version"
require "content_playlist_creation/playlist"
require "content_playlist_creation/models/attributes"
require "content_playlist_creation/models/content"
require "content_playlist_creation/models/preroll"
require "content_playlist_creation/models/video"

VALID_JSON = 'spec/fixtures/valid_input.json'
playlist = ContentPlaylistCreation.load(VALID_JSON)
puts "Input: 'MI3', 'CA'"
puts "Output: #{playlist.get_valid_playlists('MI3', 'CA')}"
puts "--"
puts "Input: 'MI3', 'UK'"
puts "Output: #{playlist.get_valid_playlists('MI3', 'UK')}"
