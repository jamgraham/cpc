require "spec_helper"

RSpec.describe ContentPlaylistCreation do

  VALID_JSON   = "#{File.dirname(__FILE__)}/fixtures/valid_input.json"
  INVALID_JSON = "#{File.dirname(__FILE__)}/fixtures/invalid_input.json"

  ContentPlaylistCreation.load(VALID_JSON)

  before(:each) do
    @playlist = ContentPlaylistCreation.get_playlist(VALID_JSON)
  end

  it "has a version number" do
    expect(ContentPlaylistCreation::VERSION).not_to be nil
  end

  describe "#load" do
    it "load video model with valid json" do
      expect(@playlist.class).to eq Playlist
    end

    it "load video model with invalid json" do
      expect{ContentPlaylistCreation.load(INVALID_JSON)}.to raise_error(NoMethodError)
    end

  end
  describe "#get_valid_playlists" do
    describe "valid" do
      # - [ ] Write a program that will take as input a content identifier and return a list of valid playlists applicable.
      # Input (MI3, CA)
      # Output
      #   Playlist1
      #   {V5, V1}
      it "should return valid playlist for 'MI3', 'CA' input" do
        result = @playlist.get_valid_playlists('MI3', 'CA')
        expect(result).to eq [["V5", "V1"]]
      end
      #Input (MI3, UK)
      # Output
      #   Playlist1
      #   {V6, V2}
      #   Playlist2
      #   {V7, V3}
      it "should return valid playlist for 'MI3', 'UK' input" do
        result = @playlist.get_valid_playlists('MI3', 'UK')
        expect(result).to eq [["V6", "V2"], ["V7", "V3"]]
      end

      it "should return valid playlist for 'XI9', 'BA' input" do
        result = @playlist.get_valid_playlists('XI9', 'BA')
        expect(result).to eq [["V23", "V13"]]
      end
    end

    describe "invalid" do
      # Input (MI3, US)
      # Output
      #   (No legal playlist possible because the Pre-Roll Video isn't compatible with the aspect ratio of the Content Video for the US)
      it "should return invalid playlist for 'MI3', 'UK' input" do
        result = @playlist.get_valid_playlists('MI3', 'US')
        expect(result).to eq "(No legal playlist possible because the Pre-Roll Video isn't compatible with the aspect ratio of the Content Video for the US)"
      end

      it "should return ArgumentError for invalid country for 'MI3', 'XX' input" do
        expect{@playlist.get_valid_playlists('MI3', 'XX')}.to raise_error(ArgumentError)
      end

      it "should return ArgumentError for invalid playlist for 'MI3', 'XX' input" do
        expect{@playlist.get_valid_playlists('XX', 'XX')}.to raise_error(ArgumentError)
      end

      it "should return ArgumentError for nil input" do
        expect{@playlist.get_valid_playlists}.to raise_error(ArgumentError)
      end

    end

  end

end
